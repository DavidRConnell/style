function plot(varargin)
%PLOT restyle current figure for documentation.
%   PLOT alter current figures attributes to fit Rush scheme for use in documentation.
%   PLOT(PALETTE) Style the plot using color palette PALETTE. PALETTE must be
%   one of the values defined in function PALETTE.
%
%   PLOT(COLORARRAY) color the plot's children with the colors in COLORARRAY.
%   There should be at least one color for each child; otherwise MATLAB's
%   default color palette is silently used.
%
%   PLOT(..., 'legend', LEGEND) label plot children with the members of cell
%   array LEGEND.
% 
%   TODO
%   PLOT(..., 'handle', FIGUREHANDLE) style the specified FIGUREHANDLE
%   instead of the current axis.
%
%   See also PALETTE, GCA, +LATEXTOOLS/SAVEFIGURE.

    p = inputParser;
    defaultColors = style.palette('default');
    validStringCell = @(x) iscell(x) && utils.iscellall(x, @ischar);
    validColorArrayOrPalette = @(x) size(x, 2) == 3 || isstring(x) || ischar(x);

    p.addOptional('colors', defaultColors, validColorArrayOrPalette);
    p.addParameter('legend', [], validStringCell);
    p.parse(varargin{:});

    if isstring(p.Results.colors) || ischar(p.Results.colors)
        colorOrder = style.palette(p.Results.colors);
    else
        colorOrder = p.Results.colors;
    end

    fontsize = 16;

    set(gcf, 'InvertHardcopy', 'off')
    set(gca, 'FontSize', fontsize)

    switch get(gca, 'Type')
      case 'axes'
        editplot;
      case 'heatmap'
        editheatmap;
      otherwise
        error('Style:UnknownFigureType', ...
              'Figure type %s not yet implemented.', figureType)
    end

    function editplot
        set(gca, 'Box', 'off')
        pbaspect([1 1 1])
        children = get(gca, 'Children');
        getColor = @(idx) colorOrder(mod(idx - 1, length(colorOrder)) + 1, :);

        if length(children) < length(colorOrder)
            colorOrder = colorOrder(2:end);
        end

        if length(children) <= length(colorOrder)
            for i = 1:length(children)
                styleChild(children(length(children) - i + 1), getColor(i));
            end
        end

        if ~isempty(p.Results.legend)
            addLegend(children, p.Results.legend);
        end

    end

    function styleChild(child, color)
        type = split(class(child), '.');
        switch type{end}
            case 'Line'
              child.Color = color;
              child.LineWidth = 1;
            case 'Bar'
              child.FaceColor = color;
              child.EdgeColor = color;
        end
    end

    function addLegend(children, legend)
        assert(length(legend) <= length(children), 'Too many legend items.')
        hold on
        for i = 1:length(legend)
            str = sprintf('\\color[rgb]{%f, %f, %f}\\fontsize{%d}%s', ...
                          children(i).Color, fontsize, legend{i});

            x = children(i).XData;
            x = x(end) + ((x(end) - x(1)) / 50);
            y = children(i).YData;
            y = y(end);
            text(x, y, str)
        end
        hold off
    end

    function editheatmap
        dataLength = length(get(gca, 'XDisplayLabels'));
        labels = repmat({''}, [dataLength, 1]);
        set(gca, 'XDisplayLabels', labels,...
                 'YDisplayLabels', labels)
        set(gca, 'InnerPosition', [0.05 0.075 0.85 0.85])
        colormap default

        nColors = length(unique(get(gca, 'ColorData')));
        if nColors <= 5
            set(gca, 'Colormap', makeColorMap(nColors))
            set(gca, 'ColorbarVisible', 'off')
        end

        function cmap = makeColorMap(nColors)
            cmap = ones(nColors, 3);
            colorOrder = [[1 1 1]; [0.4, 0.4, 0.4]; secondary; tertiary; quaternary];
            getColor = @(idx) colorOrder(mod(idx - 1, length(colorOrder)) + 1, :);
            for c = 1:nColors
                cmap(c, :) = getColor(c);
            end
        end
    end
end
