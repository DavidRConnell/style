function [p, bg] = palette(name)
%PALETTE get a theme's color palette
%   P = PALETTE get the default MATLAB color palette.
%   P = PALETTE(NAME) get the named color palette, NAME. NAME can have the
%   values RUSH, TUFTE, or DEFAULT. PALETTE(DEFAULT) is equivalent to PALETTE().
%
%   [P, BG] = PALETTE(...) get the background color for the theme.

    assert(isstring(name) || ischar(name), 'Style:BadArgument', ...
           'Argument must be a string or character vector');

    if nargin == 0
        name = 'Default';
    end

    switch lower(name)
      case 'rush'
        blue = [67 96 146];
        green = [29 111 66];
        red = [157 67 41];
        yellow = [242 205 0];
        bg = [1 1 1];
        p = [blue; red; green; yellow] / 255;
      case 'tufte'
        red = [160 0 0];
        orange = [160 72 0];
        blue = [0 96 96];
        green = [0 128 0];
        bg = [255 255 248] / 255;
        p = [red; orange; blue; green] / 255;
      case 'default'
        bg = [1 1 1];
        p = get(gca, 'ColorOrder');
      otherwise
        error('Style:UnknownPalette', ...
              'Palette %s has not been defined.', name)
    end
end
